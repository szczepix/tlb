﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using ProtoBuf;

namespace TLB
{
    [ProtoContract]
    public class PlayerPointsStorage
    {
        [ProtoMember(1)]
        public Dictionary<long, PlayerPoints> Players;
    }

    [ProtoContract]
    public class BlockSettingsStorage
    {
        [ProtoMember(1)]
        public float Boost = 1;

        [ProtoMember(2)]
        public bool AutoTurnOff = true;
    }

    [ProtoContract]
    public class PlayerPoints
    {
        [ProtoMember(1)]
        public int Refinery;
        [ProtoMember(2)]
        public int Assembler;
        [ProtoMember(3)]
        public int Custom1;
        [ProtoMember(4)]
        public int Custom2;
        [ProtoMember(5)]
        public int Custom3;

        public int GetPointsForType(PointsType type)
        {
            switch (type)
            {
                case PointsType.Assembler:
                    return Assembler;
                case PointsType.Refinery:
                    return Refinery;
                case PointsType.Custom1:
                    return Custom1;
                case PointsType.Custom2:
                    return Custom2;
                case PointsType.Custom3:
                    return Custom3;
            }
            return -1;
        }
        public int Drain(PointsType type, int minus)
        {
            switch (type)
            {
                case PointsType.Assembler:
                    Assembler -= minus;
                    return Assembler;
                case PointsType.Refinery:
                    Refinery -= minus;
                    return Refinery;
                case PointsType.Custom1:
                    Custom1 -= minus;
                    return Custom1;
                case PointsType.Custom2:
                    Custom2 -= minus;
                    return Custom2;
                case PointsType.Custom3:
                    Custom3 -= minus;
                    return Custom3;
            }
            return -1;
        }

        public void AddAll(int add)
        {
            var max = TLBSessionCore.CurrentModSettings.MaxPoints;
            Assembler = Math.Min(Assembler + add, max.Assembler);
            Refinery = Math.Min(Refinery + add, max.Refinery);
            Custom1 = Math.Min(Custom1 + add, max.Custom1);
            Custom2 = Math.Min(Custom2 + add, max.Custom2);
            Custom3 = Math.Min(Custom3 + add, max.Custom3);
        }
    }
    
    [ProtoContract]
    public class ModSettings
    {
        [ProtoMember(1)]
        public List<BlockConfig> TLBlocks = new List<BlockConfig>();
        [ProtoMember(2)]
        public PlayerPoints MaxPoints = new PlayerPoints()
        {
            Assembler = int.MaxValue,
            Refinery = int.MaxValue,
            Custom1 = int.MaxValue,
            Custom2 = int.MaxValue,
            Custom3 = int.MaxValue
        };
    }

    [ProtoContract]
    public enum PointsType
    {
        None = -1,
        Assembler = 0,
        Refinery = 1,
        Custom1 = 2,
        Custom2 = 3,
        Custom3 = 4
    }

    [ProtoContract]
    public class BlockConfig
    { 
        [ProtoMember(1)]
        public string Matcher;
        [ProtoMember(2)]
        public PointsType PointType;
        [ProtoMember(3)]
        public int MinimalPointsForActivation;
        [ProtoMember(4)]
        public int DrainAmount;
        [ProtoMember(5)]
        public bool CanBeBoosted;
        [ProtoMember(6)]
        public float MinBoost = 1;
        [ProtoMember(7)]
        public float MaxBoost = 12f;
    }
}
