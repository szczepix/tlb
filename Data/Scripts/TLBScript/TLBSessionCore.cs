﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.Game;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces.Terminal;
using SpaceEngineers.Game.Entities.Blocks;
using SpaceEngineers.Game.ModAPI;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.Utils;


namespace TLB
{
    //H2o gen
    //Farm
    //SolarPower
    //Reactor

    //TLB - Time Limited Block
    [MySessionComponentDescriptor(MyUpdateOrder.AfterSimulation)]
    public class TLBSessionCore : MySessionComponentBase
    {
        public static Guid GUID = new Guid("c480872e-fa2f-48b7-b866-acf01935c174");
        public const int UPGRADE_VALUES_CHANGED = 82733000;
        private const string DATASTORAGE_KEY = "TimeLimitedProductionBlocks";
        public static Dictionary<long, PlayerPoints> PlayersData = new Dictionary<long, PlayerPoints>();
        public static ModSettings CurrentModSettings = new ModSettings();
        public static HashSet<long> playersConnected = new HashSet<long>();
        public static bool m_controlsCreated;

        private static List<IMyTerminalControlTextbox> GUIControls = new List<IMyTerminalControlTextbox>();
        public static bool IsServer => MyAPIGateway.Session.IsServer;
        public static bool IsDedicated => MyAPIGateway.Utilities.IsDedicated;
        public static MyTerminalPageEnum lastGUI = MyTerminalPageEnum.None;
        private int timer = 0;

        public override void Init(MyObjectBuilder_SessionComponent sessionComponent)
        {
            base.Init(sessionComponent);
            //WriteToLog($"Init TLB Session core. IsServer:{IsServer}");
            // LoadModConfig(); //too early
            // LoadPlayerPoints();//too early

            TimeLimitedBlock.sync = new Scripts.Shared.Sync<BlockSettingsStorage, TimeLimitedBlock>(55593, (x)=>x.settings, TimeLimitedBlock.Handler);
        }

        public override void BeforeStart()
        {
            base.BeforeStart();
            LoadPlayerPoints();
            Networking.Init(IsServer);
            if (IsServer)
            {
                LoadModConfig();
            }
            else
            {
                Networking.RequestServerConfig();
            }

            if (!IsDedicated)
            {
                MyAPIGateway.TerminalControls.CustomControlGetter += (b, controls) =>
                {
                    controls.FindAndMove(0, (x) => x.Id.StartsWith("TLBControl") && x.Id.EndsWith("AutoTurnOff"));
                    controls.FindAndMove(0, (x) => x.Id.StartsWith("TLBControl") && x.Id.EndsWith("BoostPicker"));
                    controls.FindAndMove(0, (x) => x.Id.StartsWith("TLBControl") && x.Id.EndsWith("PointsLabel"));
                };
            }
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateBeforeSimulation();
            try
            {
                timer++;

                if (timer % 60 == 0 && TLBSessionCore.CurrentModSettings.MaxPoints != null)
                {
                    foreach (var x in PlayersData)
                    {
                        var v = x.Value;
                        v.AddAll(60);
                    }
                }

                if (!IsDedicated) //client or single-player
                {
                    var currentGUI = MyAPIGateway.Gui.GetCurrentScreen;
                    if (lastGUI != currentGUI)
                    {
                        lastGUI = currentGUI;
                        if (MyAPIGateway.Gui.GetCurrentScreen == MyTerminalPageEnum.ControlPanel)
                        {
                            if (!IsServer)
                            {
                                Networking.RequestValuesAround();
                            }
                        }
                    }

                    if (MyAPIGateway.Gui.GetCurrentScreen == MyTerminalPageEnum.ControlPanel)
                    {
                        if (timer % 60 == 0)
                        {
                            if (!IsServer)
                            {
                                Networking.RequestValuesAround();
                            }

                            foreach (var control in GUIControls)
                            {
                                control?.UpdateVisual();
                            }
                        }
                    }
                }
            }catch(Exception ex)
            {
                WriteToLog($"TLB: UpdateAfterSimulation error, send logs to Foogs! ex:[{ex}]");
            }
        }

        public override void SaveData()
        {
            base.SaveData();
            //WriteToLog("SaveData");
            SavePlayerPoints();
        }

        #region helpers
        public static void CreateControls()
        {
            try
            {
                //TLBSessionCore.WriteToLog("m_controlsCreated()");
                CreateControl<IMyRefinery>();
                CreateControl<IMyAssembler>();
                CreateControl<IMyGasGenerator>();
                CreateControl<IMySolarPanel>();

                CreateaAutoTurnOffCheckbox<IMyRefinery>();
                CreateaAutoTurnOffCheckbox<IMyAssembler>();
                CreateaAutoTurnOffCheckbox<IMyGasGenerator>();
                CreateaAutoTurnOffCheckbox<IMySolarPanel>();

                CreateBoostPicker<IMyRefinery>();
                CreateBoostPicker<IMyAssembler>();
                CreateBoostPicker<IMyGasGenerator>();
                //CreateControl<MyWindTurbine>();
                m_controlsCreated = true;
            }
            catch (Exception ex)
            {
                WriteToLog($"TLB: CreateControls error, send logs to Foogs! ex:[{ex}]");
            }
        }

        private static void CreateControl<Z>()
        {
                IMyTerminalControlTextbox Control = MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlTextbox, Z>($"TLBControl_{typeof(Z).ToString()}_" + "PointsLabel");
                Control.Title = MyStringId.GetOrCompute("Production points");
                Control.Tooltip = MyStringId.GetOrCompute("You earn production points each second.");
                Control.Getter = (block) =>
                {
                    try
                    {
                        var logic = block.GetAs<TimeLimitedBlock>();
                        if (logic == null) return new StringBuilder();
                        var PlayerPoints = TLBSessionCore.GetPlayerSettings(block.SlimBlock.BuiltBy);
                        double timePoints = PlayerPoints.GetPointsForType(logic.UsePointsType);
                        return new StringBuilder(timePoints < 0 ? "-" : "" + TimeSpan.FromSeconds(timePoints / 60d).ToString(@"d\.hh\:mm\:ss") + " | POINT-GROUP:" + (int)logic.UsePointsType);
                    } catch (Exception e)
                    {
                        WriteToLog(block.BlockDefinition.SubtypeName + " " + block.SlimBlock.BuiltBy + " " + e.ToString());
                        return new StringBuilder();
                    }
                };
                Control.Enabled = (block) => false;
                Control.Visible = (block) =>
                {
                    var gl = block.GetAs<TimeLimitedBlock>();
                    return gl != null && gl.IsTimeLimitedBlock();
                };
                MyAPIGateway.TerminalControls.AddControl<Z>(Control);
                GUIControls.Add(Control);
        }

        private static void CreateBoostPicker<Z>()
        {
            IMyTerminalControlSlider Control = MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlSlider, Z>($"TLBControl_{typeof(Z).ToString()}_" + "BoostPicker");
            Control.Title = MyStringId.GetOrCompute("Boost multiplier");
            Control.Tooltip = MyStringId.GetOrCompute("How much you to spend points");
            Control.SetLimits (1f, 12f);
            Control.SupportsMultipleBlocks = true;
            Control.Writer = (block, sb) =>
            {
                var logic = block.GetAs<TimeLimitedBlock>();
                if (logic == null) return;
                sb.Append (logic.settings.Boost);
            };
            Control.Getter = (block) =>
            {
                var logic = block.GetAs<TimeLimitedBlock>();
                if (logic == null) return 0;
                var boost = logic.settings.Boost;
                return boost;
            };
            Control.Setter = (block, v) =>
            {
                var logic = block.GetAs<TimeLimitedBlock>();
                if (logic == null) return;
                logic.GUISetBoostMutliplier(v);
            };
            Control.Enabled = (block) => true;
            Control.Visible = (block) =>
            {
                var gl = block.GetAs<TimeLimitedBlock>();
                return gl != null && gl.IsTimeLimitedBlock() && gl.m_config != null && gl.m_config.CanBeBoosted;
            };
            MyAPIGateway.TerminalControls.AddControl<Z>(Control);
        }

        private static void CreateaAutoTurnOffCheckbox<Z>()
        {
            IMyTerminalControlCheckbox Control = MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlCheckbox, Z>($"TLBControl_{typeof(Z).ToString()}_" + "AutoTurnOff");
            Control.Title = MyStringId.GetOrCompute("Auto Turn Off multiplier");
            Control.Tooltip = MyStringId.GetOrCompute("If block can't work properly it will be automaticly turned off");
            Control.SupportsMultipleBlocks = true;
            Control.Getter = (block) =>
            {
                var logic = block.GetAs<TimeLimitedBlock>();
                if (logic == null) return false;
                var boost = logic.settings.AutoTurnOff;
                return boost;
            };
            Control.Setter = (block, v) =>
            {
                var logic = block.GetAs<TimeLimitedBlock>();
                if (logic == null) return;
                logic.GUISetAutoTurnOff(v);
            };
            Control.Enabled = (block) => true;
            Control.Visible = (block) =>
            {
                var gl = block.GetAs<TimeLimitedBlock>();
                return gl != null && gl.IsTimeLimitedBlock() && gl.m_config != null && gl.m_config.CanBeBoosted;
            };
            MyAPIGateway.TerminalControls.AddControl<Z>(Control);
        }


        private void LoadModConfig()
        {
            try
            {
                if (MyAPIGateway.Utilities.FileExistsInWorldStorage("TLBModSettings.xml", typeof(ModSettings)))
                {
                    using (var reader = MyAPIGateway.Utilities.ReadFileInWorldStorage("TLBModSettings.xml", typeof(ModSettings)))
                    {
                        CurrentModSettings = MyAPIGateway.Utilities.SerializeFromXML<ModSettings>(reader.ReadToEnd());
                        ///WriteToLog("ModSettings successfully loaded from file.");
                    }
                }
                else
                {
                    //WriteToLog("ModSettings not found, using default.");
                    var SECOND = 60; //ticks
                    var FRAMES_100 = 100; //ticks
                    var MINUTE = 60; //ticks
                    var TWO_DAYS = 2 * 24 * 3600 * SECOND;

                    CurrentModSettings.MaxPoints.Refinery = TWO_DAYS;
                    CurrentModSettings.MaxPoints.Assembler = TWO_DAYS;
                    CurrentModSettings.MaxPoints.Custom1 = TWO_DAYS;
                    CurrentModSettings.MaxPoints.Custom2 = TWO_DAYS;
                    CurrentModSettings.MaxPoints.Custom3 = TWO_DAYS;
                    //DrainAmount is called each 100 

                    //DRAIN = 5184000 / (Average production time in seconds * Amount of working blocks) / Was limit of blocks * 100 / 60
                    //

                    CurrentModSettings.TLBlocks.Add(new BlockConfig() { DrainAmount = 600, MinimalPointsForActivation = MINUTE, PointType = PointsType.Refinery, Matcher = "Refinery/*" });
                    CurrentModSettings.TLBlocks.Add(new BlockConfig() { DrainAmount = 600, MinimalPointsForActivation = MINUTE, PointType = PointsType.Assembler, Matcher = "Assembler/*" });
                    CurrentModSettings.TLBlocks.Add(new BlockConfig() { DrainAmount = 150, MinimalPointsForActivation = MINUTE, PointType = PointsType.Custom1, Matcher = "OxygenGenerator/*" });
                    //CurrentModSettings.TLBlocks.Add(new BlockConfig() { DrainAmount = 600, MinimalPointsForActivation = 3600, PointType = PointsType.Custom2, Matcher = "SolarPanel/*" });
                    //CurrentModSettings.TLBlocks.Add(new BlockConfig() { DrainAmount = 600, MinimalPointsForActivation = 3600, PointType = PointsType.Custom2, Matcher = "WindTurbine/*" });
                    //CurrentModSettings.TLBlocks.Add(new BlockConfig() { DrainAmount = 600, MinimalPointsForActivation = 3600, PointType = PointsType.Custom3, Matcher = "Welder/*" });
                    using (var writer = MyAPIGateway.Utilities.WriteFileInWorldStorage("TLBModSettings.xml", typeof(ModSettings)))
                    {
                        writer.Write(MyAPIGateway.Utilities.SerializeToXML(CurrentModSettings));
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToLog("Exception while loading ModSettings, using default." + ex.ToString());
            }
        }

        private void LoadPlayerPoints()
        {
            try
            {
                if (IsServer)
                {
                    string sdata;
                    MyAPIGateway.Utilities.GetVariable(DATASTORAGE_KEY, out sdata);
                    if (!string.IsNullOrWhiteSpace(sdata))
                    {
                        var bytes = Convert.FromBase64String(sdata);
                        var settings = MyAPIGateway.Utilities.SerializeFromBinary<PlayerPointsStorage>(bytes);
                        var identities = new List<IMyIdentity>();
                        MyAPIGateway.Players.GetAllIdentites(identities);
                        var players = identities.Select((x) => x.IdentityId).ToHashSet();
                        var toDelete = new List<long>();
                        foreach (var x in settings.Players)
                        {
                            if (!players.Contains(x.Key))
                            {
                                toDelete.Add(x.Key);
                            }
                        }

                        foreach (var x in toDelete)
                        {
                            settings.Players.Remove(x);
                        }
                        
                        PlayersData = settings.Players;
                    }
                    else
                    {
                        InitPlayersPoints();
                    }
                }
                else
                {
                    InitPlayersPoints();
                }
            }
            catch (Exception ex)
            {
                WriteToLog(ex.ToString());
            }
        }

        private static void InitPlayersPoints()
        {
            var settings = new Dictionary<long, PlayerPoints>();
            var identities = new List<IMyIdentity>();
            MyAPIGateway.Players.GetAllIdentites(identities);
            var players = identities.ToHashSet();
            foreach (var x in players)
            {
                settings.Add(x.IdentityId, new PlayerPoints());
            }
            PlayersData = settings;
            //WriteToLog("LoadPlayerPoints end, inited");
        }

        public static PlayerPoints GetPlayerSettings(long builtBy)
        {
            if (!PlayersData.ContainsKey(builtBy))
            {
                PlayersData[builtBy] = new PlayerPoints();
            }
            return PlayersData[builtBy];
        }

        private void SavePlayerPoints()
        {
            if (IsServer)
            {
                var settings = new PlayerPointsStorage();
                settings.Players = PlayersData;
                var bytes = MyAPIGateway.Utilities.SerializeToBinary(settings);
                MyAPIGateway.Utilities.SetVariable(DATASTORAGE_KEY, Convert.ToBase64String(bytes));
            }
        }


        /// <summary>
        /// For track my bad debug messages ;)
        /// </summary>
        /// <param name="msg"></param>
        public static void WriteToLog(string msg)
        {
            MyLog.Default.Info($"[TLB] : {msg}");
            //MyVisualScriptLogicProvider.SendChatMessage($"[TLB] : {msg}");
        }
        #endregion
    }
}
