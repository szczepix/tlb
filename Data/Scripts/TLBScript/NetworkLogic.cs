﻿using ProtoBuf;
using Sandbox.ModAPI;
using System;
using System.Collections.Generic;

namespace TLB
{
    public static class Networking
    {
        private const int PORT_CLIENT = 12338;
        private const int PORT_SERVER = 12339;
        private static bool IsServer = false;

        public static void Init(bool imServer)
        {
            IsServer = imServer;
            if (IsServer)
            {
                MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(PORT_SERVER, HandleMessage);
            }
            else
            {
                MyAPIGateway.Multiplayer.RegisterSecureMessageHandler(PORT_CLIENT, HandleMessage);
            }
        }
        static void HandleMessage(ushort channelId, byte[] bytes, ulong fromSteamID, bool useTCP)
        {
            try
            {
                var type = (MessageType)bytes[0];
                //TLBSessionCore.WriteToLog($"Received message from [{fromSteamID}]: {bytes[0]}: {type.ToString()}");
                var data = new byte[bytes.Length - 1];
                Array.Copy(bytes, 1, data, 0, data.Length);

                switch (type)
                {
                    case MessageType.ModSettings:
                        OnSettings(fromSteamID, data);
                        break;
                    case MessageType.PlayerPoints:
                        OnPointsData(fromSteamID, data);
                        break;
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                TLBSessionCore.WriteToLog($"HandleServer exception! {ex}");
            }
        }

        #region Server
        static void SendToClient(MessageType type, byte[] bytes, ulong recipient)
        {
            try
            {
                var bytesToSend = new byte[bytes.Length + 1];
                bytesToSend[0] = (byte)type;
                bytes.CopyTo(bytesToSend, 1);
                //TLBSessionCore.WriteToLog($"Sending message to {recipient}: {type}");
                MyAPIGateway.Multiplayer.SendMessageTo(PORT_CLIENT, bytesToSend, recipient);
            }
            catch (Exception ex) { TLBSessionCore.WriteToLog($"SendToClient exception:{ex}"); }
        }
        #endregion

        #region Client
        public static void RequestValuesAround()
        {
            try
            {
                var builders = new HashSet<long>();
                foreach (var x in TimeLimitedBlock.BlocksInViewField)
                {
                    builders.Add(x.BuiltBy);
                }
                SendToServer(MessageType.PlayerPoints, MyAPIGateway.Utilities.SerializeToBinary(builders));
            }
            catch (Exception ex) { TLBSessionCore.WriteToLog($"RequestValuesAround exception:{ex}"); }
        }

        public static void RequestServerConfig()
        {
            try
            {
                SendToServer(MessageType.ModSettings, new byte[0]);
            }
            catch (Exception ex) { TLBSessionCore.WriteToLog($"RequestServerConfig exception:{ex}"); }
        }

        static void SendToServer(MessageType type, byte[] bytes)
        {
            var bytesToSend = new byte[bytes.Length + 1];
            bytesToSend[0] = (byte)type;
            bytes.CopyTo(bytesToSend, 1);
            MyAPIGateway.Multiplayer.SendMessageToServer(PORT_SERVER, bytesToSend, false);
        }
        #endregion

        #region Helpers
        static void OnPointsData(ulong TargetSteamID, byte[] data)
        {
            try
            {
                if (IsServer)
                {
                    var set = MyAPIGateway.Utilities.SerializeFromBinary<HashSet<long>>(data);
                    var d = new Dictionary<long, PlayerPoints>();
                    foreach (var x in set)
                    {
                        if (TLBSessionCore.PlayersData.ContainsKey(x))
                        {
                            d.Add(x, TLBSessionCore.PlayersData[x]);
                        }
                        else
                        {
                            d.Add(x, new PlayerPoints());
                        }
                    }
                    SendToClient(MessageType.PlayerPoints, MyAPIGateway.Utilities.SerializeToBinary(d), TargetSteamID);
                    //TLBSessionCore.WriteToLog("Sended values to server.");
                }
                else
                {
                    var set = MyAPIGateway.Utilities.SerializeFromBinary<Dictionary<long, PlayerPoints>>(data);
                    foreach (var x in set)
                    {
                        TLBSessionCore.PlayersData[x.Key] = x.Value;
                        //TLBSessionCore.WriteToLog("from server x " + x.Key + " points " + x.Value.AssemblerPoints);
                    }
                    //TLBSessionCore.WriteToLog("Corrected local values from server end. " + TLBSessionCore.PlayersData.Count);
                }
            }
            catch (Exception ex) { TLBSessionCore.WriteToLog($"HandleClient exception:{ex}"); }
        }

        static void OnSettings(ulong TargetSteamID, byte[] bytes)
        {
            try
            {
                if (IsServer)
                {
                    var settingsPacked = MyAPIGateway.Utilities.SerializeToBinary<ModSettings>(TLBSessionCore.CurrentModSettings);
                    SendToClient(MessageType.ModSettings,settingsPacked, TargetSteamID);

                    //TLBSessionCore.WriteToLog($"New settigs sended to client.");
                }
                else
                {
                    TLBSessionCore.CurrentModSettings = MyAPIGateway.Utilities.SerializeFromBinary<ModSettings>(bytes);
                    //TLBSessionCore.WriteToLog($"New settigs from server recieved.");
                }
            }
            catch (Exception ex) { TLBSessionCore.WriteToLog($"OnSettings exception:{ex}"); }
        }

        [ProtoContract]
        enum MessageType : byte
        {
            ModSettings,
            PlayerPoints
        }
        #endregion
    }
}